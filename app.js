const puppeteer = require('puppeteer');
const OSS = require('ali-oss');
const Pool = require('pg').Pool
require('dotenv').config()
const fs = require('fs');

const client = new Pool({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  password: process.env.DB_PASS,
  port: process.env.DB_PORT,
  callbackWaitsForEmptyEventLoop: false
});

/** Create function for upload file to OSS */
const uploadFile = async (pathFile, file) => {
  const client = new OSS({
    region: 'oss-ap-southeast-5',
    accessKeyId: 'LTAI5tGrLNtEgNQETtGhjXm6',
    accessKeySecret: 'Sb6iONXEJRPdNV7wHUEXNQTg5EAVAP',
    bucket: 'studioranks',
  });

  try {
    const result = await client.put(pathFile, file);
    return result;
  } catch (err) {
    return err;
  }
};

/** Create function for check file has uplaoded */
const checkFile = async (pathFile) => {
  const client = new OSS({
    region: 'oss-ap-southeast-5',
    accessKeyId: 'LTAI5tGrLNtEgNQETtGhjXm6',
    accessKeySecret: 'Sb6iONXEJRPdNV7wHUEXNQTg5EAVAP',
    bucket: 'studioranks',
  });

  try {
    const result = await client.get(pathFile);
    return result;
  } catch (err) {
    return err;
  }
};

/** Create function for Puppeter download file */
const downloadFile = async (url, nameFile) => {
  const browser = await puppeteer.launch({
      args: ['--no-sandbox'],
      headless: true
  })
  const page = await browser.newPage();
  await page.goto(url, { waitUntil: 'networkidle2' });
  await page.pdf({
      path: `./pdf/${nameFile}.pdf`,
      format: 'A4',
      printBackground: true
  });

  await browser.close();
};

const mainProcess = async () => {
  console.log ('Start process');
  /** Get data from database */
  const query = {
    text: 'SELECT * FROM test_user WHERE status = $1 AND file_report IS NULL',
    values: ['3'],
  }
  const result = await client.query(query);

  /** Looping data from database */
  for (let i = 0; i < result.rows.length; i++) {
    /** Create name file */
    const nameFile = result.rows[i].name.replace(/[^a-zA-Z0-9 ]/g, "").replace(/\s/g, '-')  + '-' + result.rows[i].id + Date.now();
    let pathFile = `result/${nameFile}.pdf`;

    /** Download file */
    await downloadFile('https://app.studiorank.xyz/rslt/' + result.rows[i].token, nameFile);

    /** Upload file to OSS */
    const upload = await uploadFile(pathFile, `./pdf/${nameFile}.pdf`);
    console.log('Process upload');

    /** Update status file to database */
    if (upload.res.status === 200) {
      const queryUpdate = {
        text: 'UPDATE test_user SET file_report = $1 WHERE id = $2',
        values: [pathFile, result.rows[i].id],
      }
      await client.query(queryUpdate);
      console.log('Success upload file');
      /** Remove file in `./pdf/${nameFile}.pdf` */
      fs.unlink(`./pdf/${nameFile}.pdf`, (err) => {
        if (err) {
          console.error(err)
          return
        }
      })
    }
  }
};

/** Unlimited looping for mainProcess function and delay 5 minutes after end and start again */
const main = async () => {
  await mainProcess();
  setTimeout(() => {
    main();
  }, 300000);
};

main();